from telethon import TelegramClient, events
from telethon.tl.custom import Button
from telethon.tl.types import MessageMediaDocument
import json
from urllib.request import urlopen 
from urllib.parse import urlencode
from LiteSQL import lsql
from time import time
import asyncio


#======== Вход


sql = lsql("echo")  
cursor = sql.get_cursor()
conn = sql.get_connect()

client = TelegramClient("echo", 
    "",
    "").start(bot_token=":") 
client.parse_mode = 'html'


#======== Переменные + БД


cd = 30 # куллдаун 
condition = asyncio.Condition()
admins = []
vips = []
last = ""
commands = [
"/start@echoall2bot", "/help@echoall2bot", "source@echoall2bot", "/menu@echoall2bot", "/editadm@echoall2bot", "/del@echoall2bot", "/delete@echoall2bot", "/say@echoall2bot", "/mute@echoall2bot", "/edit@echoall2bot", "/pin@echoall2bot", "/get_id@echoall2bot", "/get_db@echoall2bot", "/tag@echoall2bot",
"/start", "/help", "/source", "/menu", "/editadm", "/del", "/delete", "/say", "/mute", "/edit", "/pin", "/get_id", "/get_db", "/tag"
]
que = 0

list_of_user = sql.get_all_data()
for i in range(len(list_of_user)):
    list_of_user[i] = list_of_user[i][0]
list_of_user = list(set(list_of_user))
removed = []
srtlist = "author, " + ', '.join(["id"+str(i) for i in list_of_user]) + ""
cursor.execute(f"CREATE TABLE IF NOT EXISTS albums (id, time, anon, podp, podpanon)")
cursor.execute(f"CREATE TABLE IF NOT EXISTS mess ({srtlist})")
conn.commit()


#======== Функции


def get_admins():
    global admins
    with open("admins.txt", "r") as file:
        admins = list(map(int, file.read().split()))

def get_vips():
    global vips
    with open("vips.txt", "r") as file:
        vips = list(map(int, file.read().split()))

def command(*args):
    return '|'.join([i for i in [
        rf'\/{i}(@echoall2bot)?' for i in args
    ]])

async def new_user(user):
    if user not in list_of_user:
        if len(sql.select_data(int(user), 'id')) == 0:
            sql.insert_data([(int(user), 0, 1, 0, str(user))], 5)
        if user not in removed:
            cursor.execute(f"ALTER TABLE mess ADD id{user}")
            conn.commit()
        list_of_user.append(user)

async def echo_all(author, users, message, inline, replies=None):
    global que
    while (que): 
       await asyncio.sleep(0.2)
    que = 1
    users_coroutines = list()
    mess_id = []
    for user in users:
        if user != 1658709401:
            users_coroutines.append([
                asyncio.create_task(client.send_message(
                    user, message, buttons=inline, reply_to=(replies[user] if replies is not None else replies)
                )), 
                user
            ])
    for send in users_coroutines:
        try:
            mess = await send[0]
            mess_id.append([send[1], mess.id])
        except Exception as e:
            if str(e)[:31] in ["An invalid Peer was used. Make ", "Bots can't send messages to oth", "Could not find the input entity", "User is blocked (caused by Send", "The specified user was deleted "]:
                removed.append(send[1])
                list_of_user.remove(send[1])
    column =  ', '.join(["id"+str(i[0]) for i in mess_id])
    values =  ', '.join([str(i[1]) for i in mess_id])
    cursor.execute(f"INSERT INTO mess ({column}, author) VALUES ({values}, {author})")
    conn.commit()
    await asyncio.sleep(3)
    que = 0

def get_mess_ids(userid, messid):
    txt = ', '.join(["id"+str(i) for i in list_of_user])
    data = cursor.execute(f"SELECT {txt}, author FROM mess WHERE id{userid}={messid}").fetchall()
    conn.commit()
    if data != [] and data != [()]:
        return dict(zip(list_of_user + ["author"], list(data[0])))
    return None

#======== Бот


class Telegraph:
    def make_request(method, **params):
        url = 'https://api.telegra.ph/' + method
        params = {k: v if isinstance(v, str) else json.dumps(v) for k, v in params.items()}
        r = json.loads(urlopen(url + '?' + urlencode(params)).read())
        if not r['ok']:
            raise ValueError(str(r))
        return r['result']

    def get_access_token(short_name):
        return Telegraph.make_request('createAccount',
            short_name=short_name)['access_token']
            
    def create_page(**params):
        return Telegraph.make_request('createPage', **params)

class Bot:
    @client.on(events.NewMessage())
    async def send(event):
        global last
        get_admins()
        get_vips()
        if event.message.from_id != None:
            mfi = event.message.from_id.user_id
        else:
            mfi = event.chat.id
        await new_user(mfi)
        Flag = False
        reply_ids = None
        us = sql.select_data(mfi, "id")[0]
        if (event.message.message == "" or event.message.message.split()[0] not in commands) and not event.message.out and mfi != 1658709401:
            rawtext = event.message.text.replace(" ","").lower()
            for i in ['href=', "@", "https://", "http://", "t.me/", ".com", ".ru", "tg://"]:
                if i in rawtext:
                    if us[3] != 0 or (us[0] in vips or us[0] in admins):
                        sql.edit_data("id", mfi, "podp", us[3]-1)
                    else:
                        await client.send_message(event.chat.id, "У тебя не осталось прав на отправку сообщений с сылками!\nКупить безлимитное количество отправок сообщений с сылками можно за 30 руб у @Error_mak25")
                        Flag = True
                    break
            Flag2 = False
            for i in range(3, len(event.message.message), 3):
                if event.message.message.count(event.message.message[i-3:i]) > 7:
                    Flag2 = True
                    break
            if (Flag2 or len(event.message.message) >= 3000 or event.message.message == last and event.message.message != "") and (us[0] not in admins and us[0] not in vips):
                await client.send_message(event.chat.id, "Сообщение слишком большое (боллее 3000 символов), одинаково с предыдущим или содержит повторяющиеся элементы")
                Flag = True
            # if type(event.media) == MessageMediaDocument and ('video' in dir(event.media) and event.media.video) and not (us[0] in vips or us[0] in admins):
            #     await event.reply("Из-за отдельных личностей, распространяющих плохой контент, отправка видео временно отключена")
            #     return
            if not Flag:
                if event.message.message not in [None, ""]:
                    last = event.message.message
                time1 = time()
                if int(us[1])+cd <= time1 or us[0] in admins or us[0] in vips:
                    sql.edit_data("id", mfi, "time", time1)
                    inline = []
                    if us[2] == 0:
                        if us[0] in admins:
                            txt = f"ADMIN {us[4]}"
                        elif us[0] in vips:
                            txt = f"VIP {us[4]}"
                        else:
                            txt = f"{us[4]}"
                        inline.append(Button.url(txt, f'https://{us[4]}.t.me/') if not str(us[4]).isdigit() else Button.inline(txt))
                    if event.message.is_reply:
                        reply = await event.message.get_reply_message()
                        reply_ids = get_mess_ids(mfi, reply.id)
                        if reply_ids is None:
                            if len(f"n{reply.message}".encode("utf-8")) <= 64:
                                inline.append(Button.inline(f"{reply.message[:20]}...", f"n{reply.message}"))
                            else:
                                tok = Telegraph.get_access_token("bot")
                                link = Telegraph.create_page(access_token=tok, content=[{"tag": "p", "children": [reply.message]}], title="reply")
                                inline.append(Button.url(f"{reply.message[:20]}...", link["url"]))
                    mess = await client.send_message(event.chat.id, "Ваше сообщение прошло проверку на спам и готово к отправке. Отправляем...")
                    if inline == []: inline = None
                    t = time()
                    await echo_all(us[0], list_of_user, event.message, inline, reply_ids)
                    await mess.edit(f"Ваше сообщение было отправлено {len(list_of_user)} пользователям за {round(time()-t, 2)} секунд!")
                else:
                    await client.send_message(event.chat.id, f"Вы уже отправляли сообщение! Отправить следующее можно через {int((cd - time1 + int(us[1]))//60)}\
 минут(-у) {int((cd - time1 + int(us[1]))%60)} секунд(-ы).", reply_to=event.message)

    @client.on(events.NewMessage(pattern=command("start", "help")))
    async def start(event):
        get_admins()
        await client.send_message(event.chat.id, "Просто отправь мне сообщение\n/source - исходный код бота\n/menu - меню настроек\n/edit новый текст (реплаем) - редактирование сообщения\n/delete причина (реплаем) - удаление сообщения\n/tag - включить / выключить подпись сообщений\n\nПользовательское соглашение - https://telegra.ph/Polzovatelskie-soglashenie-v2-08-23")

    @client.on(events.CallbackQuery())
    async def callback(event):
        txt = event.data.decode("utf-8")
        user = await event.get_sender()
        await new_user(user.id)
        us = sql.select_data(user.id, "id")[0]
        if txt[0] == "a":
            sql.edit_data("id", user.id, "anon", int(not us[2]))
            await client.send_message(event.chat.id, f"Теперь ваши сообщения {'НЕ' if not us[2] else ''} будут подписываться!")
        if txt[0] == "s":
            if us[4] is None or us[4][0].isdigit():
                sql.edit_data("id", user.id, "podpanon", user.username)
                await client.send_message(event.chat.id, "Теперь ваши сообщения будут подписываться вашим юзернеймом, если вы не Анонимус")
            else:
                sql.edit_data("id", user.id, "podpanon", str(user.id))
                await client.send_message(event.chat.id, "Теперь ваши сообщения будут подписываться вашим id, если вы не Анонимус")
        if txt[0] == "n":
            await event.answer(txt[1:], alert=True)
            
    @client.on(events.NewMessage(pattern=command("source")))
    async def source(event):
        await client.send_message(event.chat.id, "Ссылка на репозиторий - https://gitlab.com/Ma-Mush/echoall")

    @client.on(events.NewMessage(pattern=command("tag")))
    async def tag(event):
        if event.message.from_id != None: 
            mfi = event.message.from_id.user_id
        else: 
            mfi = event.chat.id
        await new_user(mfi)
        us = sql.select_data(mfi, "id")[0]
        sql.edit_data("id", mfi, "anon", int(not us[2]))
        await client.send_message(event.chat.id, f"Теперь ваши сообщения {'НЕ' if not us[2] else ''} будут подписываться!")

    @client.on(events.NewMessage(pattern=command("menu")))
    async def menu(event):
        if event.message.from_id != None: 
            mfi = event.message.from_id.user_id
        else: 
            mfi = event.chat.id
        await new_user(mfi)
        us = sql.select_data(mfi, "id")[0]
        await client.send_message(
            event.chat.id, "Меню настроек пользователя (для изменения - нажмите)",
            buttons=[
                [Button.inline(f"Ваше сообщение {'не' if us[2] == 1 else ''} будет подписано", "a")],
                [Button.inline(f"Подпись (id/username) - {us[4]}", "s")],
                [Button.url(f"{us[3]} сообщений с сылкой (купить)", "t.me/Error_mak25")]
        ])

    @client.on(events.NewMessage(pattern=command("editadm")))
    async def editadm(event):
        if event.from_id in admins or event.chat_id in admins:
            txt = event.message.message.split()
            sql.edit_data(txt[1], int(txt[2]), txt[3], int(txt[4]))
            for admin in admins:
                await client.send_message(admin, f"New log: {txt[1]}, {txt[2]}, {txt[3]}, {txt[4]}")
    
    @client.on(events.NewMessage(pattern=command("del", "delete")))
    async def delete(event):
        if event.message.is_reply:
            reply = await event.message.get_reply_message()
            sender = await event.get_sender()
            mess_id = get_mess_ids(sender.id, reply.id)
            if sender.id in admins or mess_id["author"] == sender.id:
                reason = event.message.message[event.message.message.find(" ")+1:]
                message = "Ваше сообщение удалено " + ("отправителем" if sender.id not in admins else f"админом {'@' + sender.username if sender.username is not None else sender.id}")
                if reason != event.message.message:
                    message += f" по причине:\n{reason}"
                if mess_id is not None:
                    delete = []
                    t = time()
                    for user, messid in mess_id.items():
                        delete.append(asyncio.create_task(client.delete_messages(user, messid)))
                    for task in delete:
                        try:
                            await task
                        except:
                            pass
                    if sender.id in admins:
                        m = await client.send_message(mess_id["author"], message)
                        await m.pin()
                    await event.reply(f"Данное сообщение было удалено за {round(time() - t, 2)} секунд")
                else:
                    await event.reply("Ошибка, этого сообщения нет в базе")
            else:
                await event.reply("Это не твое сообщение!")
        else:
            await event.reply("/del причина (реплаем на сообщение)")
    
    @client.on(events.NewMessage(pattern=command("say")))
    async def say(event):
        sender = await event.get_sender()
        if sender.id in admins and event.message.is_reply:
            reason = event.message.message[event.message.message.find(" ")+1:]
            if reason != event.message.message:
                message = f"Сообщение от админа <a href='tg://user?id={sender.id}'>{sender.first_name}</a>:\n"+reason
                reply = await event.message.get_reply_message()
                mess_id = get_mess_ids(sender.id, reply.id)
                if mess_id is not None:
                    try:
                        m = await client.send_message(mess_id["author"], message)
                        await m.pin()
                    except Exception as e:
                        await event.reply(f"Error - {e}")
                    else:
                        await event.reply("Выполнено")
                else:
                    await event.reply("Ошибка, этого сообщения нет в базе")
            else:
                await event.reply("/say текст")
        else:
            await event.reply("Ты не админ или не сделал реплай")

    @client.on(events.NewMessage(pattern=command("mute")))
    async def mute(event):
        sender = await event.get_sender()
        if sender.id in admins and event.message.is_reply:
            messp = event.message.message.split()
            if len(messp) > 2 and messp[1].isdigit():
                reason = event.message.message[event.message.message.find(messp[2]):]
                notification = f"Вы лишены права отправлять сообщение на {int(messp[1])} секунд админом <a href='tg://user?id={sender.id}'>{sender.first_name}</a>:\n" + reason
                reply = await event.message.get_reply_message()
                mess_id = get_mess_ids(sender.id, reply.id)
                if mess_id is not None:
                    try:
                        sql.edit_data("id", mess_id["author"], "time", int(messp[1])+time()-cd)
                        m = await client.send_message(mess_id["author"], notification)
                        await m.pin()
                    except Exception as e:
                        await event.reply(f"Error - {e}")
                    else:
                        await event.reply("Выполнено")
                else:
                    await event.reply("Ошибка, этого сообщения нет в базе")
            else:
                await event.reply("/mute время причина - например, /mute 1234 что-то плохое")
        else:
            await event.reply("Ты не админ или не сделал реплай")

    @client.on(events.NewMessage(pattern=command("edit")))
    async def edit(event):
        newmess = event.message.message[event.message.message.find(" ")+1:]
        if event.message.is_reply and newmess != event.message.message:
            sender = await event.get_sender()
            reply = await event.message.get_reply_message()
            mess_id = get_mess_ids(sender.id, reply.id)
            if mess_id is not None and mess_id["author"] == sender.id:
                edit = []
                t = time()
                for user, messid in mess_id.items():
                    edit.append(asyncio.create_task(client.edit_message(user, messid, newmess + "\n(edited)", buttons=reply.buttons)))
                for task in edit:
                    try:
                        await task
                    except:
                        pass
                await event.reply(f"Данное сообщение было изменено за {round(time() - t, 2)} секунд")
            else:
                await event.reply("Это не твое сообщение или его нет в базе")
        else:
            await event.reply("/edit новый текст (реплаем)")
    
    @client.on(events.NewMessage(pattern=command("pin")))
    async def pin(event):
        sender = await event.get_sender()
        if sender.id in admins and event.message.is_reply:
            reply = await event.message.get_reply_message()
            mess_id = get_mess_ids(sender.id, reply.id)
            if mess_id is not None:
                pin = []
                for user, messid in mess_id.items():
                    pin.append(asyncio.create_task(client.pin_message(user, messid)))
                for task in pin:
                    try:
                        await task
                    except:
                        pass
                await event.reply("Выполнено")
            else:
                await event.reply("Ошибка, этого сообщения нет в базе")
        else:
            await event.reply("Ты не админ или не сделал реплай")


client.run_until_disconnected()

